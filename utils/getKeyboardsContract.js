import { ethers } from "ethers";

import abi from "../utils/Keyboards.json"

const contractAddress = '0x24063834d051E19E45aE4f0E1Cc443FC0689F527';
const contractABI = abi.abi;

export default function getKeyboardsContract(ethereum) {
  if(ethereum) {
    const provider = new ethers.providers.Web3Provider(ethereum);
    const signer = provider.getSigner();
    return new ethers.Contract(contractAddress, contractABI, signer);
  } else {
    return undefined;
  }
}
